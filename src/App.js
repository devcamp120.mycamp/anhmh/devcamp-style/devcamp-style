const divStyle = {
  fontFamily: '-apple-system',
  fontSize: "1rem",
  fontWeight: 1.5,
  lineHeight: 1.5,
  color: "#292b2c",
  backgroundColor: "bisque",
  padding: "0 1em"
};

const wrapperStyle = {
  textAlign: "center",
  maxWidth: "800px",
  margin: "0 auto",
  border: "1px solid #e6e6e6",
  padding: "40px 25px",
  marginTop: "50px",
};

const imageStyle = {
  margin: "-90px auto 30px",
  width: "100px",
  borderRadius: "50%",
  objectFit: "cover",
  marginBottom: "0"
};

const quoteStyle = {
  lineHeight: 1.5,
  fontWeight: 300,
  marginBottom: "25px",
  fontSize: "1.375rem",
  fontStyle: "italic"
};

const nameStyle = {
  marginBottom: "0",
  fontWeight: 600,
  fontSize: "1rem",
  color: "blue"
};

function App() {
  return (
    <div
      style={divStyle}
    >
    <div
      style={wrapperStyle}
    >
      <img
        src="https://randomuser.me/api/portraits/women/48.jpg"
        alt="Tammy Stevens"
        style={imageStyle}
      />
    <div>
    <p
        style={quoteStyle}

      >
        This is one of the best developer blogs on the planet! I read it daily to improve my skills.
      </p>
    </div>
    <p
      style={nameStyle}
    >
      Tammy Stevens<span style={{color:"brown"}}> . Front End Developer</span> 
      </p>
    </div>
  </div>
  );
}

export default App;
